package com.pocketcocktails.pocketbar.hilt

import com.pocketcocktails.pocketbar.data.repository.CocktailsRepository
import com.pocketcocktails.pocketbar.data.repository.DefaultCocktailsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideMainRepositoryImpl(repository: DefaultCocktailsRepository): CocktailsRepository

}
