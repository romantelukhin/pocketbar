package com.pocketcocktails.pocketbar.ui.view.iview

interface BaseView {
    fun setupView()
    fun renderView()
}
