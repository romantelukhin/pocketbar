package com.pocketcocktails.pocketbar.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.pocketcocktails.pocketbar.data.domain.CocktailInfo
import com.pocketcocktails.pocketbar.data.domain.Ingredient
import com.pocketcocktails.pocketbar.data.dto.search.DrinkInfo

fun ImageView.load(imageAddress: String) {
    Glide.with(this)
        .load(imageAddress)
        .into(this)
}